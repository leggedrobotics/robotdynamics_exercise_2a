### Robot Dynamics - Exercise 2a
### October 2016


# Overview
Exercise 2a of the Robot Dynamics lecture focuses on the derivation of the Inverse Kinematics of a robotic arm. Additionally, the student will be asked to implement a simple motion controller which is based on the kinematics of the system.
All functions will be implemented in MATLAB. A tool is provided which visualizes the robot arm in any desired configuration.

# Initializing your workspace

To properly initialize your workspace for the exercise, just run the following script from the folder in which this text file is located:

    init_workspace_scripts();
    init_workspace_visualization();

This will add the scripts and visualization related folders and sub-folders to the MATLAB path.

# Visualization of the ABB 120 robot

After setting up your workspace, to load the visualization of the ABB 120 robot you can run the following script:

    loadVisualization.m script();

It will load an object named abbRobot, which allows you can visualize the robot in any joint space configuration q (which is a vector of 6 coordinates).
For example you can run:

    q = rand(6,1);
    abbRobot.setJointPositions(q);

You can test the visualization by running the script
    
    test_visualization();

which will load the visualization and run a simple motion  defined in joint space.
