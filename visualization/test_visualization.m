% Example script: visualizing and 

% Close all figures
close all; clear all; clc;

% Load the visualization
loadVisualization;

% Initialize a vector of generalized coordinates and the visualization
q = zeros(6,1);
abbRobot.setJointPositions(q);

% Set the sampling time (in seconds)
ts = 0.05;

% Set the duration of the visualization (in seconds)
tf = 10.0;
kf = tf/ts; % Number of iterations as a function of the duration and the sampling time

% Notify that the visualization loop is starting
disp('Starting visualization loop.');

% Run a visualization loop
for k=1:kf
    try
        % Start a timer
        startLoop = tic;
        
        % Set a desired vector of generalized coordinates.
        % NOTE: We use a discretized version of the sine function.
        q = 0.5*sin(2*pi*0.15*k*ts) * ones(6,1);
        
        % Set the generalized coordinates to the robot visualizer class
        abbRobot.setJointPositions(q);
        
        % If enough time is left, wait to try to keep the update frequency
        % stable
        residualWaitTime = ts - toc(startLoop);
        if (residualWaitTime > 0)
            pause(residualWaitTime);
        end
    catch
        disp('Error: Exiting the visualization loop.');
        break;
    end
end

% Notify the user that the script has ended.
disp('Visualization loop has ended.');
