% CLASSDEF obj = IRB120Visualization3DCLASS()
%
% -> generates a visualization of the IRB120 consisting of base and 6 moving
% links
%
% obj.load(), obj.load(fig_handle)
% -> loads the patchSets in its actual configuration in the figure
%
% obj.updat(q)
% with: * q_new = [q1,q2,q3,q4,q5,q6]  generalized joint coordinates [rad]
% -> updates the structure to the new joint angles
%
% Created by Marco Hutter on 29.8.2015
% for Matlab 2013a
% -> mahutter@ethz.ch
%
%
classdef IRB120Visualization3DCLASS < handle
    
    properties
        
        % Robot body/link configuration
        rotCenterPoint_
        rotAxis_
        
        % Figure and axes handles
        irb120Figure_
        irb120Axes_
        
        % Graphics handle arrays
        bodyObjectsArray_
        bodyFrameObjectsArray_
        bodyHgTransformArray_
            
    end
    
    methods
        function obj = IRB120Visualization3DCLASS()
            
            % Configure robot visualization data
            mat.FaceColor = [210,112,16]/255;
            mat.EdgeColor = 'none';
            mat.FaceLighting = 'phong';
            mat.AmbientStrength = 0.3;
            mat.DiffuseStrength = 0.3;
            mat.SpecularStrength = 1;
            mat.SpecularExponent = 25;
            mat.SpecularColorReflectance = 0.5;
            
            % Define joint DoFs and patch offsets
            obj.rotCenterPoint_{1}=[0,0,0.145];
            obj.rotAxis_{1}=[0,0,1];
            obj.rotCenterPoint_{2}=[0,0,0.290];
            obj.rotAxis_{2}=[0,1,0];
            obj.rotCenterPoint_{3}=[0,0,0.560];
            obj.rotAxis_{3}=[0,1,0];
            obj.rotCenterPoint_{4}=[0.151,0,0.630];
            obj.rotAxis_{4}=[1,0,0];
            obj.rotCenterPoint_{5}=[0.302,0,0.630];
            obj.rotAxis_{5}=[0,1,0];
            obj.rotCenterPoint_{6}=[0.372,0,0.630];
            obj.rotAxis_{6}=[1,0,0];

            % Temporary figures - will not be visible
            fig = figure('visible','off');
            ax = axes('Parent',fig);
            
            NB = 7;
            obj.bodyHgTransformArray_ = cell(1,NB);
            for i=1:NB
                obj.bodyHgTransformArray_{i} = hgtransform('Parent',ax);
            end
            
            % Generate patches with correct zero offsets
            obj.bodyObjectsArray_{1} = PatchCLASS('visualization/STLs/base.stl',mat,[0 0 0]);
            %obj.bodyFrameObjectsArray_{1} = CoordinateFrameCLASS('B');
            for i=1:6
                obj.bodyObjectsArray_{i+1} = PatchCLASS(['visualization/STLs/link',num2str(i),'.stl'],mat,obj.rotCenterPoint_{i});
                %obj.bodyFrameObjectsArray_{i+1} = CoordinateFrameCLASS(num2str(i));
            end
            
            % Only plot the end-effector frame
            obj.bodyFrameObjectsArray_{7} = CoordinateFrameCLASS('E');
            
            clear ax;
            clear fig;
            
        end
        
        function [] = reset(obj)
            for i=1:length(obj.bodyObjectsArray_)
                obj.bodyObjectsArray_{i}.reset();
            end            
        end
        
        function [] = load(obj,varargin)
            
            % load() or load(fig_handle)
            if nargin==2
                obj.irb120Figure_ = figure(varargin{1});
            else
                obj.irb120Figure_ = figure('Name','3D visualization of IRB120','Position',[100 100 800 600]);
            end
            
            obj.irb120Axes_ = axes('parent', obj.irb120Figure_);
            set(obj.irb120Figure_,'Color',[1 1 1]);
            set(obj.irb120Axes_,'Color',[1 1 1]);
            axis equal;
            axis off;
            axis vis3d;
            
            % Initialize body transforms for links which move
            for i=1:7
                set(obj.bodyHgTransformArray_{i}, 'Parent', obj.irb120Axes_);
                set(obj.bodyObjectsArray_{i}.p_, 'Parent', obj.bodyHgTransformArray_{i});
                %set(obj.bodyFrameObjectsArray_{i}.csfg_, 'Parent', obj.bodyHgTransformArray_{i});
            end
            
            % Attach the end-effector coordinate frame to the hg transform.
            set(obj.bodyFrameObjectsArray_{7}.csfg_, 'Parent', obj.bodyHgTransformArray_{7});
            
            % Initialize body graphics object positions
            bodyinit = getTransformI0();
            set(obj.bodyHgTransformArray_{1}, 'matrix', bodyinit);
            
            bodyinit = bodyinit*jointToTransform01(zeros(6,1));
            set(obj.bodyHgTransformArray_{2}, 'matrix', bodyinit);
            
            bodyinit = bodyinit*jointToTransform12(zeros(6,1));
            set(obj.bodyHgTransformArray_{3}, 'matrix', bodyinit);
            
            bodyinit = bodyinit*jointToTransform23(zeros(6,1));
            set(obj.bodyHgTransformArray_{4}, 'matrix', bodyinit);
            
            bodyinit = bodyinit*jointToTransform34(zeros(6,1));
            set(obj.bodyHgTransformArray_{5}, 'matrix', bodyinit);
            
            bodyinit = bodyinit*jointToTransform45(zeros(6,1));
            set(obj.bodyHgTransformArray_{6}, 'matrix', bodyinit);
            
            bodyinit = bodyinit*jointToTransform56(zeros(6,1));
            set(obj.bodyHgTransformArray_{7}, 'matrix', bodyinit);
           
            % Enable the visualization of the links
            for i=1:7
                set(obj.bodyHgTransformArray_{i}, 'visible', 'on');
                obj.bodyObjectsArray_{i}.load();
                %obj.bodyFrameObjectsArray_{i}.load();
            end
            
            % Only load the end-effector transform
            obj.bodyFrameObjectsArray_{7}.load();
            
        end
      
        function [] = setJointPositions(obj, q)
            
            % move all elements to the correct new position
            if length(q)~=length(obj.rotAxis_)
                error('Wrong dimension of q, it should be length 6.');
            end
            
            % Initialize temporary body transform 
            btf = eye(4);

            % Use succesive homogeneous-transformations to position each 
            % body in the visualization
            btf = btf*jointToTransform01(q);
            set(obj.bodyHgTransformArray_{2}, 'matrix', btf);
            
            btf = btf*jointToTransform12(q);
            set(obj.bodyHgTransformArray_{3}, 'matrix', btf);
            
            btf = btf*jointToTransform23(q);
            set(obj.bodyHgTransformArray_{4}, 'matrix', btf);
            
            btf = btf*jointToTransform34(q);
            set(obj.bodyHgTransformArray_{5}, 'matrix', btf);
            
            btf = btf*jointToTransform45(q);
            set(obj.bodyHgTransformArray_{6}, 'matrix', btf);
            
            btf = btf*jointToTransform56(q);
            set(obj.bodyHgTransformArray_{7}, 'matrix', btf);
            
            % Update figure/axes data
            drawnow;
            
        end
        
    end
    
end


