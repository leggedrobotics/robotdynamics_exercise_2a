% Dynamic model simulation script

% Close all figures
close all; clear;

% Load the visualization
loadVisualization;

% Initialize a vector of generalized coordinates and the visualization
q_0 = zeros(6,1);
abbRobot.setJointPositions(q_0);

% Set the default simulation duration (in seconds)
tf = 10.0;

% Notify that the simulation is being loaded
disp('Opening Simulink model..');

% Open simulink model
open('abb_dynamics.mdl');
