classdef CoordinateFrameCLASS < handle
    
   properties
       
        name_
        csf_
        csfg_
       
   end
    
    methods
        
        function obj = CoordinateFrameCLASS(varargin)
            
            % Set the csf name
            name = varargin{1};
            obj.name_ = name;
            
            % Process input arguments
            arrow_length = 0.1;
            csf_x = 0; 
            csf_y = 0;
            csf_z = 0;
            csf_u = arrow_length*[0 1 0 0];
            csf_v = arrow_length*[0 0 1 0];
            csf_w = arrow_length*[0 0 0 1];

            % Temporary figures - will not be visible
            fig = figure('visible','off');
            ax = axes('Parent',fig);
            
            obj.csfg_ = hgtransform('Parent',ax);
            obj.csf_ = cell(7,1);
            
            % Add & initialize graphics objects to figure
            hold on;
            h = quiver3(ax,csf_x,csf_y,csf_z,csf_u(2),csf_v(2),csf_w(2),0,'visible','off');
            obj.csf_{1} = h;
            h = quiver3(ax,csf_x,csf_y,csf_z,csf_u(3),csf_v(3),csf_w(3),0,'visible','off');
            obj.csf_{2} = h;
            h = quiver3(ax,csf_x,csf_y,csf_z,csf_u(4),csf_v(4),csf_w(4),0,'visible','off');
            obj.csf_{3} = h;
            hold off;

            % Configure quiver frame format properties
            obj.csf_{1}.Color = 'red';
            obj.csf_{2}.Color = 'blue';
            obj.csf_{3}.Color = 'green';
            obj.csf_{1}.LineWidth = 1.5;
            obj.csf_{2}.LineWidth = 1.5;
            obj.csf_{3}.LineWidth = 1.5;
            obj.csf_{1}.MaxHeadSize = 0.2;
            obj.csf_{2}.MaxHeadSize = 0.2;
            obj.csf_{3}.MaxHeadSize = 0.2;

            % Render latex labels for csf arrows
            randmax = arrow_length/20;
            randoff = (randmax)*rand(4);
            csf_xlabel_pos = [csf_x;csf_x;csf_x;csf_x]+csf_u.'+randoff(1);
            csf_ylabel_pos = [csf_y;csf_y;csf_y;csf_y]+csf_v.'+randoff(2);
            csf_zlabel_pos = [csf_z;csf_z;csf_z;csf_z]+csf_w.'+randoff(3);

            % Create labels
            csf_olabel = strcat('$O_{',name,'}$');
            csf_xlabel = strcat('${\bf e}^{',name,'}_{x}$');
            csf_ylabel = strcat('${\bf e}^{',name,'}_{y}$');
            csf_zlabel = strcat('${\bf e}^{',name,'}_{z}$');
            csf_labels = {{csf_olabel},{csf_xlabel},{csf_ylabel},{csf_zlabel}};

            % Render the csf lebels
            h = text(csf_xlabel_pos,csf_ylabel_pos,csf_zlabel_pos,csf_labels, ...
                'Parent', ax, ...
                'Interpreter','latex', ...
                'FontSize',12, ...
                'Visible','off');
            obj.csf_{4} = h(1);
            obj.csf_{5} = h(2);
            obj.csf_{6} = h(3);
            obj.csf_{7} = h(4);
            
            for i=1:7
               set(obj.csf_{i},'Parent',obj.csfg_); 
            end
            
            clear ax;
            clear fig;
            
        end
        
        function [] = load (obj)
            % enables patch object rendering
            for i=1:7
                set(obj.csf_{i},'visible','on');
            end
            
        end
        
    end
    
end
