
% Initialize generalized coordinates and the kinematic and dynamic
% parameters.
gen_cor = generate_gen_cor;
dyn = generate_dyn;
kin = generate_kin(gen_cor);

% Generate jacobians.
jac = generate_jac(gen_cor, kin, dyn);
eom = generate_eom(gen_cor, kin, dyn, jac);
