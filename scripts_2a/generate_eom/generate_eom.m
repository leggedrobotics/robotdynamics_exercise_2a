% generate equations of motion
function eom = generate_eom(gen_cor, kin, dyn, jac)
% By calling:
%   eom = generate_eom(gen_cor, kin, dyn, jac)
% a struct 'eom' is returned that contains the matrices and vectors
% necessary to compute the equations of motion. These are additionally
% converted to matlab scripts.

%% Setup
phi = gen_cor.phi;
dphi = gen_cor.dphi;

T_Ik = kin.T_Ik;
R_Ik = kin.R_Ik;

k_I_s = dyn.k_I_s;
m = dyn.m;
I_g_acc = dyn.I_g_acc;
k_r_ks = dyn.k_r_ks;

I_Jp_s = jac.I_Jp_s;
I_Jr = jac.I_Jr;

eom.M = sym(zeros(6,6));
eom.g = sym(zeros(6,1));
eom.b = sym(zeros(6,1));
eom.hamiltonian = sym(zeros(1,1));


%% Compute mass matrix
fprintf('Computing mass matrix M... ');
M = ...;
fprintf('done!\n');


%% Compute gravity terms
fprintf('Computing gravity vector g... ');
g = ...;
fprintf('done!\n');


%% Compute nonlinear terms vector
fprintf('Computing coriolis and centrifugal vector b and simplifying... ');
b = ...;
fprintf('done!\n');


%% Compute energy
fprintf('Computing total energy... ');
hamiltonian = ...;
fprintf('done!\n');


%% Generate matlab functions
fprintf('Generating eom scripts... ');
fprintf('M... ');
matlabFunction(M, 'vars', {phi}, 'file', 'M_fun');
fprintf('g... ');
matlabFunction(g, 'vars', {phi}, 'file', 'g_fun');
fprintf('b... ');
matlabFunction(b, 'vars', {phi, dphi}, 'file', 'b_fun');
fprintf('hamiltonian... ');
matlabFunction(hamiltonian, 'vars', {phi, dphi}, 'file', 'hamiltonian_fun');
fprintf('done!\n');


%% Store the expressions
eom.M = M;
eom.g = g;
eom.b = b;
eom.hamiltonian = hamiltonian;
eom.enPot = enPot;
eom.enKin = enKin;

end