% generate generalized coordinates
function gen_cor = generate_gen_cor()
% By calling:
%   gen_cor = generate_gen_cor()
% a struct 'gen_cor' is returned that contains the ABB IRB 120 joint state
% in symbolic form.

%% Generalized coordinates

syms phi1 phi2 phi3 phi4 phi5 phi6 'real';
syms dphi1 dphi2 dphi3 dphi4 dphi5 dphi6 'real';

phi = [phi1 phi2 phi3 phi4 phi5 phi6]';
dphi = [dphi1 dphi2 dphi3 dphi4 dphi5 dphi6]';

gen_cor.phi = phi;
gen_cor.dphi = dphi;

end