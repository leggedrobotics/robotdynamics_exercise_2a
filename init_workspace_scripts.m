% Add folders to path
addpath([pwd '/scripts_1a_solution']);
addpath([pwd '/scripts_1b_solution']);
addpath([pwd '/scripts_1c_solution']);

addpath([pwd '/scripts_2a']);
addpath([pwd '/scripts_2a/generate_eom']);
addpath([pwd '/scripts_2a/irb120_dynamics']);
