function [ q_command ] = kinematicMotionControl( ts,q_current,r_des,v_des )
% Inputs:
%  ts         : simulation time-step.
%  q_current  : current configuration of the robot
%  r_des      : desired end-effector linear position
%  v_des      : desired end-effector linear velocity
% Output: joint-space state of the robot to send to the visualization.

% TODO: User defined linear position gain
K_p = 1.0;

% TODO: User defined pseudo-inverse damping coefficient
lambda = 0.1;

% Compute the updated joint velocities. This would be used for a velocity controllable robot
r_current = jointToPosition(q_current);
J_current = jointToPosJac(q_current);
v_command = v_des + K_p*(r_des - r_current);
Dq_command = pseudoInverseMat(J_current, lambda) * v_command;

% Time integration step to update visualization. This would also be used for a position controllable robot
q_command = q_current + Dq_command*ts;
end
